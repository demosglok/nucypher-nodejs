# Sample nodejs interface for interacting with NuCypher MockNet


## Usage Instructions:

### Docker
1. docker run -it nucypherjs /bin/bash
2. inside docker container run "npm start"

### Run without docker
1. Install `pipenv` if you don't get have it and spin up a virtual environment with it:
    - `sudo pip3 install pipenv`
    - `pipenv install --dev --python 3.6` (or whatever version of python3 you have)
    - `pipenv shell`

   P.S. If you use Windows, your installation process might be more involved than that.

2. Use `nucypher.js` file from this nucypher folder.
    - See the `demo.js` file for an example of how it's used.

3. Run node demo.js to see how it works and if tests are passing

## Flow Description
Nucypher-nodejs runs nucypher-python/rpc.py as separate process and interacts with it via IPC (stdin/stdout) encoding data in json

## API Description:
require or import nucypher.js
all functions return promises, with data requested
1. `nucypher.genkey` -- Creates a pair or public and private key

@return promise(object) - with privkey and pubkey fields

2. `nucypher.grant` -- Creates a mock policy on the mocked network. This will return a string with a `policy_id`. You will use this policy ID to reencrypt and revoke the policy.

@param string alice_privkey - alice private key, alice - persont granting permissions
@param string alice_signing_privkey - alice signing private key (separate private key from first one)
@param string bob_pubkey - bobs public key, bob - person to have permission to decrypt data
@return promise(string) - policy_id , id of policy with permissions

3. `nukypher.encrypt` -- Encrypts data with supplied key (public key) and returns ciphertext and capsule.

@param string pubkey - public key to use for encryption
@param string data - data to be encrypted
@return promise(object) - with ciphertext and capsule (to be used later)

4. `nukypher.reencrypt` -- Re-encrypts a Capsule `M` times on the mock network. This requires a policy id, a min number of re-encryptions specified during `pre.split_rekey`, and a capsule object. This returns a list of capsule frags for Bob to attach to his capsule and use during `pre.decrypt`.

@param string alice_pubkey - alice public key
@param string alice_signing_pubkey - alice signing public key (separate public key from first one)
@param string bob_pubkey - bobs public key
@param string bob_capsule - capsule, returned from encryption step, used to gather fragments
@param string policy_id - id of policy, returned by grant method
@return promise(array) - array of fragments, necessary for decryption

5. `nukypher.decrypt` -- Decrypts a Capsule `M` using list of capsule frags for Bob to attach to his capsule and Bobs private key.

@param string alice_pubkey - alice public key
@param string alice_signing_pubkey - alice signing public key (separate public key from first one)
@param string bob_privkey - bobs private key,
@param string bob_capsule - capsule, returned from encryption step, used to gather fragments
@param string cfrags - array of fragments
@param string ciphertext - encrypted text to be decrypted
@return promise(string) - decrypted text


6. `nucypher.revoke` -- Revokes a policy from the network and makes re-encryptions impossible. This makes the `MockNetwork` object delete the stored kfrags stored on it per `policy_id`.

* @param string policy_id - policy_id returned by grant
* @return promise(void)
