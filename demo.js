const nu = require('./nucypher-nodejs/nucypher.js');

(async() => {
  const alice_keys = await nu.genkey();
  console.log('alice_keys', alice_keys);
  const alice_signing_keys = await nu.genkey();
  console.log('alice signing keys', alice_signing_keys);
  const bob_keys = await nu.genkey();
  console.log('bob_keys', bob_keys);
  const plaintext = 'some interesting text';
  console.log('plaintext', plaintext);
  const encrypted = await nu.encrypt(alice_keys.pubkey, plaintext);
  console.log('encrypted', encrypted);
  const policy_id = await nu.grant(alice_keys.privkey, alice_signing_keys.privkey, bob_keys.pubkey);
  console.log('policy_id', policy_id);
  const cfrags = await nu.reencrypt(
    alice_keys.pubkey,
    alice_signing_keys.pubkey,
    bob_keys.pubkey,
    encrypted.capsule,
    policy_id
  );
  console.log('cfrags', cfrags);
  const decrypted = await nu.decrypt(
    alice_keys.pubkey,
    alice_signing_keys.pubkey,
    bob_keys.privkey,
    encrypted.capsule,
    cfrags,
    encrypted.ciphertext
  )
  console.log('decrypted', decrypted)
  if(decrypted === plaintext) {
    console.log('encryption & decryption successfull')
  } else {
    console.log('test failed, encrypte text differs from original')
  }
  await nu.revoke(policy_id);
  console.log('revoked access');
  try {
     await nu.reencrypt(
      alice_keys.pubkey,
      alice_signing_keys.pubkey,
      bob_keys.pubkey,
      encrypted.capsule,
      policy_id
    );
    console.log('reencrypt after revoking succeded, this is incorrect, test failed');
  } catch (ex) {
    console.log('Expected exception after revoking access and attemting to reencrypt');
    console.log('Test passed');
  }

})().catch(ex => console.log('sequence error', ex.message));
