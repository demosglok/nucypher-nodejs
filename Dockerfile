FROM ubuntu:18.04
#FROM python:3.6

WORKDIR /usr/src/app

COPY . .

RUN apt-get update -y && apt-get install -y nodejs npm python3-pip git
# locales

RUN npm i

# RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
#    locale-gen
# ENV LANG en_US.UTF-8
# ENV LANGUAGE en_US:en
# ENV LC_ALL en_US.UTF-8

ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

RUN pip3 install pipenv
RUN pipenv install --dev --python 3.6
#RUN pipenv shell

CMD ["node", "demo.js"]
